import { Server } from './src/server.js';
import { App } from './src/app.js';
import { LoggerService } from './src/shared/logger.js';
import { MongoService } from './src/shared/mongo.js';
import { dbConfig, appConfig } from './src/config/config.js';

const loggerService = new LoggerService();
const databaseService = new MongoService(dbConfig, loggerService);
const application = new App(appConfig, databaseService, loggerService);
const server = new Server(process, application, loggerService);

server.start();
