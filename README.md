vSpace API

Development

1. Start up a local mongo server (Docker needed):
docker run --rm -it -v /data/db:/mnt/d/Code/sources/vspace/mongodata -p 27017:27017 --name mongodb -d mongo
2. Install the app dependencies:
npm install
3. Start the app in dev mode:
npm run dev
