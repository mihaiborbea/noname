const _appConfig: IAppConfig = {
  port: process.env.PORT ? parseInt(process.env.PORT, 10) : 3000,
  env: process.env.NODE_ENV || 'development',
  serviceName: process.env.SERVICE_NAME || 'vSpace',
};
export const appConfig = Object.freeze(_appConfig);

const _dbConfig: IDatabaseConfig = {
  prefix: process.env.DB_PREFIX || 'mongodb://',
  host: process.env.DB_HOST || 'localhost:27017',
  username: process.env.DB_USERNAME ? encodeURIComponent(process.env.DB_USERNAME) : '',
  password: process.env.DB_PASSWORD ? encodeURIComponent(process.env.DB_PASSWORD) : '',
  database: process.env.DB_NAME || 'vspace',
};
export const dbConfig = Object.freeze(_dbConfig);

export interface IAppConfig {
  port: number;
  env: string;
  serviceName: string;
}

export interface IDatabaseConfig {
  prefix: string;
  host: string;
  username: string;
  password: string;
  database: string;
}
