import { LoggerService } from './shared/logger';
import { App } from './app';

export class Server {
  constructor(private proc: NodeJS.Process, private app: App, private logger: LoggerService) {}

  public start(): void {
    (async () => {
      await this.app.start();
    })();

    const gracefulStop = () => this.app.stop();
    const rejectionHandler = (reason) => this.logger.error(`UnhandledRejection: ${reason.stack}`);
    const exceptionHandler = (reason) => this.logger.error(`UncaughtException: ${reason.stack}`);
    const warningHandler = (warn) => this.logger.warn(`Warning: ${warn.name}`);

    this.proc.on('unhandledRejection', rejectionHandler);
    this.proc.on('uncaughtException', exceptionHandler);
    this.proc.on('warning', warningHandler);
    this.proc.on('SIGTERM', gracefulStop);
    this.proc.on('SIGINT', gracefulStop);
  }
}
