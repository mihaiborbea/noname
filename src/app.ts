import express from 'express';
import bodyParser from 'body-parser';
import { Server } from 'http';

import { IAppConfig } from './config/config.js';
import { ILog } from './shared/logger.js';
import { IDatabaseService } from './shared/mongo.js';

export class App {
  private httpServer: Server;
  private expressApp: any;

  constructor(
    private config: IAppConfig,
    private mongoService: IDatabaseService,
    private logger: ILog,
  ) {}

  public async start(): Promise<void> {
    await this.mongoService.connect();
    await this.startServer();
  }

  public async stop(): Promise<void> {
    await this.mongoService.disconnect();
    await new Promise((resolve) => (this.httpServer ? this.httpServer.close(resolve) : resolve()));
  }

  private async startServer(): Promise<void> {
    this.configExpress();
    await new Promise(async (resolve) => {
      this.httpServer = this.expressApp.listen(this.config.port, () => {
        this.logger.info(`Server started listening ...`);
        resolve();
      });
    });
  }

  private configExpress(): void {
    this.expressApp = express();
    // this.expressApp.use(
    //   bodyParser.urlencoded({
    //     extended: true,
    //   }),
    // );
    this.expressApp.use(bodyParser.json());
    this.expressApp.get('/host-info', (req, res) => {
      res.setHeader('Content-Type', 'application/json');
      res.status(200).send(JSON.stringify({ status: 'OK' }));
    });
  }
}
