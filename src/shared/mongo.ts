import MongoClient from 'mongodb';

import { IDatabaseConfig } from '../config/config.js';
import { ILog } from './logger.js';

export interface IDatabaseService {
  connection: unknown;
  connect: () => Promise<unknown>;
  disconnect: () => Promise<void>;
}

export class MongoService implements IDatabaseService {
  public connection: any;

  constructor(private config: IDatabaseConfig, private logger: ILog) {}

  public async connect(): Promise<MongoClient.MongoClient> {
    const URI = this.getConnectionURI();
    try {
      if (!this.connection) {
        this.connection = await MongoClient.connect(URI, {
          useNewUrlParser: true,
          useUnifiedTopology: true,
        });
      }
      if (this.connection) {
        this.logger.info('Connected to mongodb ...');
      }
      return this.connection;
    } catch (err) {
      this.logger.error(`Failed to connect to mongodb. ${err}`);
    }
  }

  public async disconnect(): Promise<void> {
    if (this.connection) {
      await this.connection.close();
      this.logger.info('Disconnected from mongodb ...');
      return;
    }
    this.logger.warn('No mongodb connection to close ...');
  }

  private getConnectionURI(): string {
    const credentials = this.config.password
      ? `${this.config.username}:${this.config.password}@`
      : '';
    return (
      this.config.prefix +
      credentials +
      this.config.host +
      '/' +
      this.config.database +
      '?retryWrites=true&w=majority'
    );
  }
}
