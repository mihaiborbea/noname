import winston from 'winston';
import { appConfig } from '../config/config.js';

const loggerConfig = {
  levels: {
    error: 0,
    warn: 1,
    info: 2,
    debug: 3,
  },
  colors: {
    error: 'red',
    warn: 'yellow',
    info: 'cyan',
    debug: 'green',
  },
};
export interface ILog {
  error(message: string | any): void;
  warn(message: string | any): void;
  info(message: string | any): void;
  debug(message: string | any): void;
}
export class LoggerService implements ILog {
  private logger: ILog = this.buildLogger(loggerConfig);

  public error(...message: any[]): void {
    this.log('error', message);
  }
  public warn(...message: any[]): void {
    this.log('warn', message);
  }
  public info(...message: any[]): void {
    this.log('info', message);
  }
  public debug(...message: any[]): void {
    this.log('debug', message);
  }

  private log(level: string, message: any): void {
    this.logger[level](message.join(' '));
  }

  private buildLogger(config: any): ILog {
    if (appConfig.env === 'production') {
      return this.buildProductionLogger(config);
    } else {
      return this.buildDevelopmentLogger(config);
    }
  }

  // TODO: finish dev logger
  private buildDevelopmentLogger(config: any): ILog {
    return winston.createLogger({
      levels: config.levels,
      format: winston.format.combine(
        winston.format.colorize({ all: true, colors: config.colors }),
        winston.format.timestamp(),
        winston.format.printf((info) => `[${info.timestamp}][${info.level}] ${info.message}`),
      ),
      transports: [new winston.transports.Console({ level: 'debug' })],
    });
  }

  // TODO: finish prod logger
  private buildProductionLogger(config: any): ILog {
    return winston.createLogger({
      levels: config.levels,
      format: winston.format.combine(winston.format.timestamp(), winston.format.json()),
      transports: [new winston.transports.Console({ level: 'info' })],
    });
  }
}
